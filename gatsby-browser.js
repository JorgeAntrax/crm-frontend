import React from "react"
import { AdminProvider } from "./src/context"
import "./src/styles/index.scss"

// Wraps every page in a component
export const wrapRootElement = ({ element, props }) => {
  return (<AdminProvider {...props}>{element}</AdminProvider>)
}
