import { useContext } from 'react';
import AdminContext from "@context";

const useLayout = () => {
	const { auth } = useContext(AdminContext)
	return { ...auth }
}

export default useLayout
