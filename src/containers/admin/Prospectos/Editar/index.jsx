import React, { useEffect } from "react";
import {
  Modal,
  Title,
  SubTitle,
  Text,
  Flex,
  Col,
  Input,
  Textarea,
  Select,
  Radio,
  Alert,
  Button,
  InputHelper,
} from "@components";

import { Address } from "@modules";
import { porcentajes, servicios } from "@constants";
import useAgregar from "./hooks";

const Editar = ({ defaultData, getData }) => {
  const { data, update } = useAgregar(defaultData, getData);
	console.log(defaultData);

  return (
    <>
      <SubTitle className="mt:1">Datos básicos</SubTitle>

      <Flex className="mt:2" justify="between">
        <Col md={10} className="ph:05 mb:1">
          <Input
            className="mb:1"
            value={data.name}
            placeholder="Nombre (s)"
          />
          <Input
            className="mb:1"
            value={data.lname}
            placeholder="Apellidos (s)"
          />
          <Input
            className="mb:1"
            value={data.email}
            placeholder="Correo Electrònico"
          />
          <Input
            className="mb:1"
            value={data.phone}
            placeholder="Nùmero de telefono"
          />

          <Select
            getValue={v => console.log("category", v)}
            placeholder="Servicio consultado"
            value={data.request?.id}
            options={servicios}
          />
        </Col>
        <Col md={9} className="ph:05 mb:1">
          <Textarea value={data.message} placeholder="Mensaje personalizado" />
        </Col>
      </Flex>

      <SubTitle className="mt:2">Ubicación</SubTitle>

      <Col md={10} className="mb:2">
        <Address defaultData={data.address} />
      </Col>

      <SubTitle>Negociaciones</SubTitle>

      <Flex justify="between" className="mb:2">
        <Col md={10}>
          <Input
            className="mv:1"
            value={data.factura}
            placeholder="Costo de factura sin IVA"
          />
          <InputHelper
            className="mt:15 mb:1"
            tooltip="Es el porcentaje correspondiente que se le asignará a un afiliado por recomendar a este prospecto."
          >
            <Select
              getValue={v => console.log("category", v)}
              placeholder="Porcentaje de comision"
              value={data.comision}
              options={porcentajes}
            />
          </InputHelper>
          <Input
            className="mb:1"
            value={data.comisionOtra}
            placeholder="Comisión personalizada"
          />
        </Col>
      </Flex>
    </>
  );
};

export default Editar;
