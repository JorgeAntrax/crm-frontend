import { useEffect, useState } from "react";
import defaultState from "./state";
import { negociaciones, porcentajes, servicios } from "@constants";

const useAgregar = (defaultData, getData) => {
	const [data, setData] = useState(defaultState);

	const actions = {
		getAddress: (v) => update("address", v),
	};

	const update = (key, value) => {
		if (key === "comision") {
			value = porcentajes.filter((c) => +c.id === +value)[0];
		}
		if (key === "request") {
			value = servicios.filter((c) => +c.id === +value)[0];
		}
		if (key === "negociacion") {
			value = negociaciones.filter((c) => +c.id === +value)[0];
		}

		setData({
			...data,
			[key]: value,
		});
	};

	useEffect(() => {
		if (defaultData) {
			const comision = porcentajes.filter(
				(c) => +c.id === +defaultData.comision
			)[0];
			const request = servicios.filter(
				(c) => c.label === defaultData.request
			)[0];
			const negociacion = negociaciones.filter(
				(c) => c.label === defaultData.negociacion
			)[0];
			setData({ ...defaultData, request, comision, negociacion });
		}
	}, []);

	useEffect(() => {
		if(+data.factura > 0 && +data.comision?.id > 0) {
			let cpv = 0;
			console.log(data)


			if(data.comision.id === 1) {
				cpv = +data.factura * 0.05;
			}
			if(data.comision.id === 2) {
				cpv = +data.factura * 0.10;
			}
			if(data.comision.id === 3) {
				cpv = +data.factura * 0.15;
			}

			if(+data.comisionOtra > 0) {
				cpv = +data.factura * (+data.comisionOtra / 100);
			}

			update('cpv', cpv);
		}
	}, [data.factura, data.comision, data.comisionOtra])

	useEffect(() => {
		getData && getData({ ...data });
	}, [data]);

	return { data, update, actions };
};

export default useAgregar;
