import React, { useEffect } from "react";
import {
	Modal,
	Title,
	SubTitle,
	Text,
	Flex,
	Col,
	Input,
	Textarea,
	Select,
	Radio,
	Alert,
	Button,
	InputHelper,
} from "@components";

import { Address } from "@modules";
import { negociaciones, porcentajes, servicios } from "@constants";
import useAgregar from "./hooks";

const Agregar = ({ defaultData, getValue }) => {
	const { data, update, actions } = useAgregar(defaultData, getValue);
	const { getAddress } = actions;

	return (
		<>
			<SubTitle className="mt:1">Datos básicos</SubTitle>

			<Flex className="mt:2" justify="between">
				<Col md={10} className="ph:05 mb:1">
					<Input
						className="mb:1"
						value={data.name}
						getValue={(v) => update("name", v)}
						placeholder="Nombre (s)"
						maxLength={50}
						capitalize
						onlyAlpha
					/>
					<Input
						className="mb:1"
						value={data.lname}
						getValue={(v) => update("lname", v)}
						placeholder="Apellidos (s)"
						maxLength={50}
						capitalize
						onlyAlpha
					/>
					<Input
						className="mb:1"
						value={data.email}
						getValue={(v) => update("email", v)}
						placeholder="Correo Electrònico"
						type="email"
					/>
					<Input
						className="mb:1"
						value={data.phone}
						getValue={(v) => update("phone", v)}
						placeholder="Nùmero de telefono"
						maxLength={10}
						isNumber
					/>

					<Select
						getValue={(v) => update("request", v)}
						placeholder="Servicio consultado"
						value={data.request?.id}
						options={servicios}
					/>
				</Col>
				<Col md={9} className="ph:05 mb:1">
					<Textarea
						capitalize
						value={data.message}
						getValue={(v) => update("message", v)}
						placeholder="Mensaje personalizado"
					/>
				</Col>
			</Flex>

			<SubTitle className="mt:2">Ubicación</SubTitle>
			<Col md={10} className="mb:2">
				<Address defaultData={data.address} getValue={(v) => getAddress(v)} />
			</Col>

			<SubTitle>Negociaciones</SubTitle>

			<Flex justify="between" className="mb:2">
				<Col md={10}>
					<Select
						getValue={(v) => update("negociacion", v)}
						placeholder="Estado de la negociación"
						value={data.negociacion?.id}
						options={negociaciones}
						otherLess
					/>
					<Input
						className="mv:1"
						value={data.factura ? data.factura.toString() : ''}
						getValue={(v) => update("factura", v)}
						placeholder="Costo de factura sin IVA"
						isNumber
					/>
					<InputHelper
						className="mt:15 mb:1"
						tooltip="Es el porcentaje correspondiente que se le asignará a un afiliado por recomendar a este prospecto."
					>
						<Select
							otherLess
							getValue={(v) => update("comision", v)}
							placeholder="Porcentaje de comision"
							value={data.comision?.id}
							options={porcentajes}
						/>
					</InputHelper>

					{+data?.comision?.id === 9999 && (
						<Input
							className="mb:1"
							value={data.comisionOtra}
							getValue={(v) => update("comisionOtra", v)}
							placeholder="Comisión personalizada"
							isNumber
						/>
					)}

					<Input
						className="mv:1"
						value={data.cvp ? data.cpv.toString() : ''}
						getValue={(v) => update("cpv", v)}
						placeholder="Comision de factura sin IVA"
						disabled
						isNumber
					/>
				</Col>
				<Col md={9} className="ph:05 mb:3">
					<Textarea
						getValue={(v) => update("negociacionMessage", v)}
						value={data.negociacionMessage}
						placeholder="Comentarios de la negociación"
						capitalize
					/>
				</Col>
			</Flex>
		</>
	);
};

export default Agregar;
