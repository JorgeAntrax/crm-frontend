import React, { useEffect, useState, useContext } from "react";
import { Box, Flex, Col, Text, ButtonIcon } from "@components";
import Confirm from "@components/Modal/Confirm";
import { navigate } from "gatsby";

import AdminContext from "@context";
import { getProspectos, deleteProspecto } from "@services/admin";
import { negociaciones } from "@constants";

function TableProspectos() {
	const {
		auth: { token },
		setNotify,
	} = useContext(AdminContext);

	const [prospectos, setProspectos] = useState([]);
	const [idDeleter, confirmDelete] = useState(null);

	useEffect(() => {
		getProspectos(token).then(({ data }) => {
			const newData = data.map((p) => {
				let negociacion = negociaciones.filter(
					(n) => n.label === p.negociacion
				)[0];
				if (!negociacion) {
					negociacion = negociaciones.filter((n) => +n.id === 999)[0];
				}
				return {
					...p,
					negociacion,
				};
			});
			setProspectos(newData);
		});
	}, []);

	const delProspecto = (id) => {
		deleteProspecto(id, token)
			.then(({ data }) => {
				const newData = data.map((p) => {
					let negociacion = negociaciones.filter(
						(n) => n.label === p.negociacion
					)[0];
					if (!negociacion) {
						negociacion = negociaciones.filter((n) => +n.id === 999)[0];
					}
					confirmDelete(null);
					return {
						...p,
						negociacion,
					};
				});
				setProspectos(newData);
			})
			.catch(() =>
				setNotify({
					error: true,
					message: "No hemos podido eliminar el prospecto",
				})
			);
	};

	return (
		<>
			{!!prospectos?.length &&
				prospectos.map(
					(
						{
							id,
							name,
							lname,
							country,
							state,
							request,
							recomended_by,
							source,
							negociacion,
							esCliente,
							comision,
						},
						key
					) => (
						<Box inline className="ph:1 pv:05 mb:1" key={key}>
							<Flex>
								<Col autofit className="pr:1">
									<Text fw={500} fs={14}>
										{key + 1}
									</Text>
									<Text fs={10} opacity={0.4}>
										ID
									</Text>
								</Col>
								<Col xs={3}>
									<Text fw={500} fs={14}>
										{name}
									</Text>
									<Text fs={10} opacity={0.4}>
										Nombres
									</Text>
								</Col>
								<Col xs={3}>
									<Text fw={500} fs={14}>
										{lname}
									</Text>
									<Text fs={10} opacity={0.4}>
										Apellidos
									</Text>
								</Col>
								<Col xs={2}>
									<Text fw={500} fs={14}>
										{country || "México"}
									</Text>
									<Text fs={10} opacity={0.4}>
										Región
									</Text>
								</Col>
								<Col xs={2}>
									<Text fw={500} fs={14}>
										{source || "Google"}
									</Text>
									<Text fs={10} opacity={0.4}>
										Fuente
									</Text>
								</Col>
								<Col xs={3}>
									<Text fw={500} fs={14}>
										{request || "NA"}
									</Text>
									<Text fs={10} opacity={0.4}>
										Servicio consultado
									</Text>
								</Col>
								<Col autofit className="pr:1">
									<Text fw={500} fs={14}>
										{recomended_by || "Lead Organico"}
									</Text>
									<Text fs={10} opacity={0.4}>
										Recomendado por
									</Text>
								</Col>
								<Col xs={3}>
									<Text fw={500} fs={14}>
										<span className={negociacion.style}>
											{negociacion?.label}
										</span>
									</Text>
									<Text fs={10} opacity={0.4}>
										Negociación
									</Text>
								</Col>
								<Col autofit className="t:center">
									<ButtonIcon
										onClick={() => confirmDelete(id)}
										tooltip="Eliminar"
										w={60}
										direction="bottom"
										pill
										icon="close"
									/>
								</Col>
								{esCliente === 0 && (
									<Col autofit className="t:center">
										<ButtonIcon
											onClick={() => navigate(`/admin/prospectos/${id}`)}
											tooltip="Editar"
											w={60}
											direction="bottom"
											pill
											icon="edit"
										/>
									</Col>
								)}
							</Flex>
						</Box>
					)
				)}

			{!!idDeleter && (
				<Confirm
					show={!!idDeleter}
					onClose={() => confirmDelete(null)}
					onAccept={() => {
						delProspecto(idDeleter);
					}}
				>
					¿Estas seguro de eliminar este proespecto?
				</Confirm>
			)}

			{!!!prospectos?.length && (
				<Text className="mt:2" fs={18} opacity={0.5}>
					No tienes prospectos registrados
				</Text>
			)}
		</>
	);
}

export default TableProspectos;
