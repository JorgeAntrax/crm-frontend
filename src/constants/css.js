export const breakpoints = {
	xs: 390,
	sm: 768,
	md: 1024,
	lg: 1300,
	hd: 1500
}
