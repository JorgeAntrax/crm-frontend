import React from 'react'
import { Flex } from '@components';

export default ({children, ...props}) => (
	<Flex align="center" justify="center" {...props}>
		{children}
	</Flex>
)
