import React from "react";
import { Text } from "@components"

export default ({ children, ...props }) => (
	<Text fw={700} xs={28} md={32} {...props}>
		{children}
	</Text>
)
