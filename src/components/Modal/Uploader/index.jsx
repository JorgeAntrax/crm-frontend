import React from "react";
import { Modal, Text, Center, Svg } from "@components";

import Wait from "@assets/icons/waiting.svg";
import success from "@assets/icons/success2.svg";

export default ({ show, completed }) => {
	return (
		<Modal
			unClosed
			width={500}
			show={show}
			icon={Wait}
			title="¡Porfavor se paciente!"
		>
			<div className="ph:2 pt:2">
				<Text fs={14} fw={500} line={1.4}>
					Estamos subiendo los archivos al sistema, esto tomará unos minutos, te
					avisaremos si sucede algo.
				</Text>
			</div>

			{!completed ? (
				<Center className="mt:3">
					<div className="Spinner">
						<div className="SpinnerHalf SpinnerHalf--left"></div>
						<div className="SpinnerHalf SpinnerHalf--right"></div>
					</div>
				</Center>
			) : (
				<Center className="mt:3">
					<Svg icon={success} width={60} height={60} />
				</Center>
			)}

			<Text fs={12} fw={500} align="center" opacity={0.5} className="pv:2">
				{!completed
					? "Subiendo archivos..."
					: "Se completo la carga de archivos"}
			</Text>
		</Modal>
	);
};
