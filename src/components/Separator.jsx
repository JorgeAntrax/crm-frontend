import st from 'styled-components'

export default st.div`
	display: block;
	width: 100%;
	height: 1px;
	background-color: #ececec;
	margin-bottom: 1.5rem;
`
