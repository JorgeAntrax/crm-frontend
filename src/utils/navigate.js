export default url => {
	if(typeof window !== 'undefined') {
		window.location.href = `${window.location.origin}${url}`;
	}
}
