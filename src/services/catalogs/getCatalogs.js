import { request } from "@services/api";
import endpoints from "@services/endpoints";

import { checkJWToken } from "@services";

const getCatalogs = (token) =>
	new Promise((resolve, reject) => {
		request.setAuthorization(token);
		const {
			CATALOGS: { ALL },
		} = endpoints;

		checkJWToken(token)
			.then(async () => {
				const { data } = await request.get(ALL);
				resolve(data);
			})
			.catch(({ response }) => reject(response));
	});

export default getCatalogs
