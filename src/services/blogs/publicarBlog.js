import { google } from "googleapis";
import key from "./service_account.json";

const publicarUrl = (url) => new Promise((resolve, reject) => {
	const jwtClient = new google.auth.JWT(
			key.client_email,
			null,
			key.private_key,
			["https://www.googleapis.com/auth/indexing"],
			null
	);
	
	jwtClient.authorize(function(err, tokens) {
			if (err) {
				reject(err);
				return;
			}

			fetch("https://indexing.googleapis.com/v3/urlNotifications:publish", {
				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${tokens.access_token}`
				},
				body: JSON.stringify({
					url,
					type: "URL_UPDATED"
				})
			})
			.then(r => r.json())
			.then(r => resolve(r))
			.catch(er => reject(er))
	});
})


export default publicarUrl