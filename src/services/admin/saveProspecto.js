import { request } from "@services/api";
import endpoints from "@services/endpoints";
import { checkJWToken } from "@services";

const {
  ADMIN: { SAVE_PROSPECTO },
} = endpoints;

const saveProspecto = (id, data, token) =>
  new Promise((resolve, reject) => {
		if(!+id) {
			reject({message: 'invalid id prospecto'})
		}

    request.setAuthorization(token);
    checkJWToken(token)
      .then(() => {
        request
          .post(`${SAVE_PROSPECTO}/${id}`, data)
          .then(() => resolve(true))
          .catch(err => reject(err));
      })
      .catch(({ response }) => reject(response));
  });

export default saveProspecto;
