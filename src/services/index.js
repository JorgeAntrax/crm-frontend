export { default as checkJWToken } from "./session/checkJWToken";
export { default as createUUID } from "./session/createUUID";
export { default as FingerprintJSX } from "./session/fingerprint";
export { default as Login } from "./session/login";
export { default as Logout } from "./session/logout";
export { default as activarCuenta } from "./session/activarCuenta";

//catalogs
export { default as getCatalogs } from "./catalogs/getCatalogs";
export { default as AddOption } from "./catalogs/AddOption";
export { default as getZipCode } from "./zipcode";

//properties
export { default as addBlog } from "./blogs/addBlog";
export { default as getBlog } from "./blogs/getBlog";
export { default as getBlogs } from "./blogs/getBlogs";
export { default as deleteBlog } from "./blogs/deleteBlog";
// export { default as publicarUrl } from "./blogs/publicarBlog";
export { default as uploadFiles } from "./blogs/uploadFiles";
export { default as getCategories } from "./blogs/getCategories";

//Administrador
export * from './admin'
//Afiliados
export * from './afiliados'
